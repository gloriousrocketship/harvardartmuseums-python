import harvardartmuseums as art
import api_keys as your_keys
"""
Don't forget to fill out api_keys.py with your own credentials before using these examples.

Pagination Example

This example shows how to make a pagination works in the client.

By default, a page returns ten objects in a response, you can increase this to 100 objects per page.
"""
art.register(your_keys.harvardartmuseum)


#First let's build our ContentResponse object
resp = art.get_raw(art.activities().size(11).page(4)) #Pages are one-indexed. This is the fourth page of responses containing 11 objects.
#Note the use of get_raw() instead of get(). we want the complete response, not just search results
print "\nActivities: Page {} of {}".format(resp.page(),resp.pages())
for activity in resp.content_objects(): print activity

#all responses have built in pagination, just remember to use get_raw instead of get()
#calling nextPage or prevPage will also refresh the data, making the content_objects ready to use
resp.nextPage()

#Print what's on the fifth page
print "\nNext Page: {} of {}".format(resp.page(),resp.pages())
for activity in resp.content_objects(): print activity #print all activities returned

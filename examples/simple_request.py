import harvardartmuseums as art
import api_keys as your_keys
"""
Don't forget to fill out api_keys.py with your own credentials before using these examples.

Simple Request Example

This example shows how to make a simple request (load some of the cultures registered in the Harvard Art Museum).
"""
art.register(your_keys.harvardartmuseum) #always make sure you're registered with the api before calling anything

#build a culture request: who doesn't want more culture in their life?
request  = art.cultures()   #a ContentRequest object for cultures
results = art.get(request)  #a list of Culture data objects
# this is identical to art.get_raw(request).content_objects()

for culture_obj in results : print culture_obj

import harvardartmuseums as art
import api_keys as your_keys
"""
Fill out api_keys.py with your own credentials before using.

Object Example

This example call shows how to call the Object endpoint.

https://github.com/harvardartmuseums/api-docs/blob/master/object.md
"""
art.register(your_keys.harvardartmuseum) #always make sure you're registered with the api before calling anything

#build a culture request: who doesn't want more culture in their life?
request  = art.objects().size(1)   #a ContentRequest object for cultures
results = art.get(request)  #a list of Culture data objects

# this is identical to art.get_raw(request).content_objects()
import pprint
for obj in results : pprint.pprint(obj.raw_dict)

import harvardartmuseums as art
import api_keys as your_keys
"""
Fill out api_keys.py with your own credentials before using.

Iteration Example

This example call iterates through the pages of all galleries and stores their data in a single array.

This is can be an expensive process, so consider before using it!
"""
art.register(your_keys.harvardartmuseum)

galleries_list = []

starting_page = 1
response = art.get_raw(art.galleries().page(starting_page))
print "\Galleries: Page",response.page(),"of",response.pages()
for gallery in response.content_objects(): galleries_list.append gallery #print all galleries returned

# refresh() is called with every pagination function, (nextPage, prevPage)
# content_objects()  will then contains the new objects

gallery_data = response.nextPage().content_objects()
print "\nActivities: Page",response.page(),"of",response.pages(),""
for gallery in gallery_data: print gallery #print all galleries returned from last api call

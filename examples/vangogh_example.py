import harvardartmuseums as art
import api_keys as your_keys
"""
Don't forget to fill out api_keys.py with your own credentials before using these examples.

Simple Request Example

This example shows how to make a person request (Vincent Van Gough) and then load the artwork made by that person.
"""
art.register(your_keys.harvardartmuseum)

#build a person request: q stands for query
persons_request = art.persons().q('displayname','"Vincent van Gogh"') #find Vincent van Gogh
persons_response = art.get(persons_request) #returns all person records matching "Vincent Van Gogh"
first_match = persons_response[0] #assign vanGogh to the first result.

artist_works = art.get(art.objects().q('personid', first_match.personid() ) )
# you can combine requests and responses into one, pythonic line
#(this one returns the art work Objects of this van Gogh person we found earlier)

print "Van Gogh result data:"
import pprint
pprint.pprint(first_match.data_dict) #you can pretty print the content by accessing its internal dictionary
#or you can use the helper functions
vanGogh_withpipe = vanGogh_artworks[8]
print "\nExample Artwork:\n\tTitle: {}\n\tURL: {}".format(vanGogh_withpipe.title(),vanGogh_withpipe.primaryimageurl())

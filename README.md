# Harvard Art Museums API - Python Client #

### What is it? ###
* A client for reading the online resources of the Harvard Art Museums. Their entire collection!  
* Visit http://www.harvardartmuseums.org/collections/api to apply for a key  
* See https://github.com/harvardartmuseums/api-docs for more deeper API details  

### Dependencies ###
* Python 2.7.6 (minimum) 
* [pytest 2.7.0](http://pytest.org/) required only for testing

### Usage ###
* First [Register for an api key](http://www.harvardartmuseums.org/collections/api).
* Replace `[YOUR_API_KEY]` with your own api key inside `examples/api_keys.py`
```
#!python
# examples/api_keys.py
harvardartmuseum="[YOUR_API_KEY]"

```
* That's it! You're ready to try out all the examples.

### Testing ###
Testing requires pytest:
* `pip install pytest`


### Contribute! ###
* Write at least one test for the code you add/change
* I'll look at it.
* I'll give feedback or merge it.
* **At the very least,** if you find something wrong or missing: [File an Issue](https://bitbucket.org/gloriousrocketship/harvardartmuseums-python/issues).


### Who do I talk to? ###
* email him[§]andrew-tremblay.com for questions about the Python client  
* Use [this feedback form](https://docs.google.com/forms/d/118WjSPgKEYBjLU3B3iUkELwHbgeWryVb_5hw3o6_3K8/viewform) to give feedback and suggestions about the API
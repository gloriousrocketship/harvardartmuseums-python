#py.test -q test_response.py
import pytest

import harvardartmuseums as art
from harvardartmuseums.content_response import ContentResponse

import test_api_keys as your_keys
art.register(your_keys.harvardartmuseum)


class TestResponse:
    def test_live_server_endpoints(self):
    	#this call is very expensive, consider skipping it
        self.checkRaw(art.get_raw(art.activities()))
        self.checkRaw(art.get_raw(art.centuries()))
        self.checkRaw(art.get_raw(art.classifications()))
        self.checkRaw(art.get_raw(art.colors()))
        self.checkRaw(art.get_raw(art.cultures()))
        self.checkRaw(art.get_raw(art.exhibitions()))
        self.checkRaw(art.get_raw(art.galleries()))
        self.checkRaw(art.get_raw(art.groups()))
        self.checkRaw(art.get_raw(art.mediums()))
        self.checkRaw(art.get_raw(art.objects()))
        self.checkRaw(art.get_raw(art.periods()))
        self.checkRaw(art.get_raw(art.persons()))
        self.checkRaw(art.get_raw(art.places()))
        self.checkRaw(art.get_raw(art.publications()))
        self.checkRaw(art.get_raw(art.sites()))
        self.checkRaw(art.get_raw(art.spectrums()))
        self.checkRaw(art.get_raw(art.techniques()))
        self.checkRaw(art.get_raw(art.worktypes()))

    def test_pagination(self):
    	#use an endpoint that we're SURE will have multiple pages if the endpoints work
        objects_response = art.get_raw(art.objects())
        assert objects_response.page() == 1
        objects_response.nextPage()
        assert objects_response.page() == 2
        objects_response.previousPage()
        assert objects_response.page() == 1
        objects_response.previousPage()
        assert objects_response.page() == 1
        max_page = objects_response.pages()

    def test_page_jump(self):
        page_jump = 2
        objects_response = art.get_raw(art.objects().page(page_jump))
        assert objects_response.page() == page_jump

    def test_imageURL(self):
    	#Get an image that we're sure will be there. Like this Van Gough:
    	#art.get(art.objects().objectnumber("1934.35")).imageOfUrl() #'http://nrs.harvard.edu/urn-3:HUAM:53534_dynmc'
        assert 1

    def checkRaw(self, reponseObj):
        assert reponseObj.response_dict.has_key('info')
        info_dict = reponseObj.info_dict()
        assert info_dict.has_key('page')
        assert info_dict.has_key('pages')
        assert info_dict.has_key('totalrecords')
        assert reponseObj.response_dict.has_key('records')
        assert len(reponseObj.records()) > 0
        #TODO compare static response structure to see if anything changes in the return values

class ResponseStatic(ContentResponse):
	def refresh(self):
		if self.request_data.connection.sock != None :
			self.request_data.connection.close()
		static_filename = self.endpoint() + ".json"
		#todo: load static response from a file
		self.response_raw = self.request_data.send().read().decode('utf-8')
		self.response_dict = json.loads(self.response_raw)
		return self

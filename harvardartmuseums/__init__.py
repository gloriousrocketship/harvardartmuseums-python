import errors
import globals as g
import content_request as req
import content_response as resp
import endpoints

g.client_key = None #wipe the old api_key every time we import the module
def register(api_key):
	if('API_KEY' in api_key):
		raise errors.HarvardApiError(errors.no_client_key)
	g.client_key = api_key

#the workhorses
""" Returns all response objects of the given request, good for single calls """
def get(request):
	return resp.ContentResponse(request).content_objects()
""" Returns raw response object of the given request, good for more complicated api interaction"""
def get_raw(request):
	return resp.ContentResponse(request)

#general declarations
def activities(params={}):
	return req.get_endpoint(g.ep_activity, params)
def activity(id, params={}):
	return req.get_endpoint((g.ep_activity+"/"+id), params)

def centuries(params={}):
	return req.get_endpoint(g.ep_century, params)
def century(id, params={}):
	return req.get_endpoint((g.ep_century+"/"+id), params)

def classifications(params={}):
	return req.get_endpoint(g.ep_classification, params)
def classification(id, params={}):
	return req.get_endpoint((g.ep_classification+"/"+id), params)

def colors(params={}):
	return req.get_endpoint(g.ep_color,params)
def color(id, params={}):
	return req.get_endpoint((g.ep_color+"/"+id), params)

def cultures(params={}):
	return req.get_endpoint(g.ep_culture, params)
def culture(id, params={}):
	return req.get_endpoint((g.ep_culture+"/"+id), params)

def exhibitions(params={}):
	return req.get_endpoint(g.ep_exhibition, params)
def exhibition(id, params={}):
	return req.get_endpoint((g.ep_exhibition+"/"+id), params)

def galleries(params={}):
	return req.get_endpoint(g.ep_gallery,params)
def gallery(id, params={}):
	return req.get_endpoint((g.ep_gallery+"/"+id), params)

def groups(params={}):
	return req.get_endpoint(g.ep_group, params)
def group(id, params={}):
	return req.get_endpoint((g.ep_group+"/"+id), params)

def mediums(params={}):
	return req.get_endpoint(g.ep_medium, params)
def medium(id, params={}):
	return req.get_endpoint((g.ep_medium+"/"+id), params)

def objects(params={}):
	return req.get_endpoint(g.ep_object, params)
def object(id, params={}):
	return req.get_endpoint((g.ep_object+"/"+id), params)

def periods(params={}):
	return req.get_endpoint(g.ep_period, params)
def period(id, params={}):
	return req.get_endpoint((g.ep_period+"/"+id), params)

def persons(params={}):
	return req.get_endpoint(g.ep_person, params)
def person(id, params={}):
	return req.get_endpoint((g.ep_person+"/"+id), params)

def places(params={}):
	return req.get_endpoint(g.ep_place,params)
def place(id, params={}):
	return req.get_endpoint((g.ep_place+"/"+id), params)

def publications(params={}):
	return req.get_endpoint(g.ep_publication,params)
def publication(id, params={}):
	return req.get_endpoint((g.ep_publication+"/"+id), params)

def sites(params={}):
	return req.get_endpoint(g.ep_site,params)
def site(id, params={}):
	return req.get_endpoint((g.ep_site+"/"+id), params)

def spectrums(params={}):
	return req.get_endpoint(g.ep_spectrum,params)
def spectrum(id, params={}):
	return req.get_endpoint((g.ep_spectrum+"/"+id), params)

def techniques(params={}):
	return req.get_endpoint(g.ep_technique,params)
def technique(id, params={}):
	return req.get_endpoint((g.ep_technique+"/"+id), params)

def worktypes(params={}):
	return req.get_endpoint(g.ep_worktype,params)
def worktype(id, params={}):
	return req.get_endpoint((g.ep_worktype+"/"+id), params)

# go to www.harvardartmuseums.org/collections/api for more information

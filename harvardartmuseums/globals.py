global client_key
client_key = None
endpoint = 'api.harvardartmuseums.org'

#Endpoints
ep_activity = "/activity"
ep_century = "/century"
ep_classification = "/classification"
ep_color = "/color"
ep_culture = "/culture"
ep_exhibition = "/exhibition"
ep_gallery = "/gallery"
ep_group = "/group"
ep_medium = "/medium"
ep_object = "/object"
ep_period = "/period"
ep_person = "/person"
ep_place = "/place"
ep_publication = "/publication"
ep_site = "/site"
ep_spectrum = "/spectrum"
ep_technique = "/technique"
ep_worktype = "/worktype"
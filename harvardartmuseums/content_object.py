import globals, errors
import datetime, dateutil.parser
import json

class ContentObject():
	full_response_dict = None
	data_dict = { }
	def __init__(self, total_response, response_dict):
		self.full_response_dict = total_response
		self.data_dict = response_dict

	def __str__(self):
		return json.dumps(self.data_dict)

	#TODO:  a single value, or '|' (pipe) separated list of values, or "any"
	def adaptive_set(self, key, valueOrValues="any"):
		return self.set_and_return(key, valueOrValues)

	#TODO: 1 or more terms passed to a key
	def set_at_least_one(self, key, valueOrValues):
		return self.set_and_return(key, valueOrValues)

	#"2015-04-28T04:13:38-0400" to date
	def s_to_date(self, dateString):
		d = dateutil.parser.parse(dateString)
		print d.strftime('%m/%d/%Y')
		return d

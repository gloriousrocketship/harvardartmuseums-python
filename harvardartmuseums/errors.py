class HarvardApiError(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)

no_client_key = "client key not defined, please call harvardartmuseums.register([API_KEY]) with your API key before making connections"
incorrect_value = "an incorrect value was passed to this function"

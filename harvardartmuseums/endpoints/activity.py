from ..content_object import ContentObject

class Activity(ContentObject):

	def objectid(self):
		return self.data_dict.get('objectid')

	def date(self):
		return self.data_dict.get('date')  #s_to_date

	def activitytype(self):
		return self.data_dict.get('activitytype')

	def activitycount(self):
		return self.data_dict.get('activitycount')

from ..content_object import ContentObject

class Gallery(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def galleryid(self):
		return self.data_dict.get('galleryid')

	def floor(self):
		return self.data_dict.get('floor')

	def gallerynumber(self):
		return self.data_dict.get('gallerynumber')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:07:59-0400"  #s_to_date

	def name(self):
		return self.data_dict.get('name')

	def objectcount(self):
		return self.data_dict.get('objectcount')

	def theme(self):
		return self.data_dict.get('theme')

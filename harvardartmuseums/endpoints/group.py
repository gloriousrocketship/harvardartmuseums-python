from ..content_object import ContentObject

class Group(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def groupid(self):
		return self.data_dict.get('groupid')

	def parentgroupid(self):
		return self.data_dict.get('parentgroupid')

	def haschildren(self):
		return self.data_dict.get('haschildren')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:07:59-0400"  #s_to_date

	def level(self):
		return self.data_dict.get('level')

	def name(self):
		return self.data_dict.get('name')

	def objectcount(self):
		return self.data_dict.get('objectcount')

	def pathforward(self):
		return self.data_dict.get('pathforward')

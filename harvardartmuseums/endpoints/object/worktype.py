from ...content_object import ContentObject

class ObjectWorktype(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def worktype(self):
		return self.raw_dict.get('worktype')

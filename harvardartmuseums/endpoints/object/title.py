from ...content_object import ContentObject

class ObjectTitle(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def title(self):
		return self.raw_dict.get('title')
	def titletype(self):
		return self.raw_dict.get('titletype')
	def displayorder(self):
		return self.raw_dict.get('displayorder')
	def titleid(self):
		return self.raw_dict.get('titleid')

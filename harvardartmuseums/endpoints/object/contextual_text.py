from ...content_object import ContentObject

class ObjectContextualText(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict

	def text(self):
		return self.raw_dict.get('text')

	def type(self):
		return self.raw_dict.get('type')

	def context(self):
		return self.raw_dict.get('context')

	def date(self):
		return self.raw_dict.get('date')

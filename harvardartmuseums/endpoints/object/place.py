from ...content_object import ContentObject

class ObjectPlace(ContentObject):
	def placeid(self):
		return self.data_dict.get('placeid')

	def type(self):
		return self.data_dict.get('type')

	def displayname(self):
		return self.data_dict.get('displayname')

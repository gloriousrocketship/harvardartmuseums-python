from ...content_object import ContentObject

from color import ObjectColor
from image import ObjectImage
from place import ObjectPlace
from title import ObjectTitle
from person import ObjectPerson
from gallery import ObjectGallery
from worktype import ObjectWorktype
from grouping import ObjectGrouping
from exhibition import ObjectExhibition
from publication import ObjectPublication
from contextual_text import ObjectContextualText

from details import *
from terms import *

"""
This is easily the largest class in the endpoints, if not the whole library.

See
https://github.com/harvardartmuseums/api-docs/blob/master/object.md
for more information

"""

class Object(ContentObject):
	raw_dict = { }
	gallery = None
	colors_list = [ ]
	details_list = [ ]
	images_list = [ ]
	people_list = [ ]
	worktypes_list = [ ]
	terms_list = [ ]
	publications_list = [ ]
	places_list = [ ]
	exhibitions_list = [ ]
	groupings_list = [ ]
	titles_list = [ ]
	contextual_texts = []

	def __init__(self, total_response, response_dict):
		ContentObject.__init__(self, total_response, response_dict)
		self.raw_dict = response_dict
		self.gallery = ObjectGallery(self.raw_dict.get('gallery', {}))
		for c_dict in self.raw_dict.get('colors', []):
			self.colors_list.append(ObjectColor(c_dict))
		for i_dict in self.raw_dict.get('images', []):
			self.images_list.append(ObjectImage(i_dict))
		for p_dict in self.raw_dict.get('people', []):
			self.people_list.append(ObjectPerson(p_dict))
		for w_dict in self.raw_dict.get('worktypes', []):
			self.worktypes_list.append(ObjectWorktype(w_dict))
		for p_dict in self.raw_dict.get('publications', []):
			self.publications_list.append(ObjectPublication(p_dict))
		for e_dict in self.raw_dict.get('exhibitions', []):
			self.exhibitions_list.append(ObjectExhibition(e_dict))
		for t_dict in self.raw_dict.get('contextualtext', []):
			self.exhibitions_list.append(ObjectContextualText(t_dict))

		#the only (found) subarray of details is technical details
		for d_dict in self.raw_dict.get('details', {}).get('technical', []):
			self.details_list.append(ObjectTechnicalDetails(d_dict))
		#the terms list have four sub-lists
		for t_dict in self.raw_dict.get('terms', {}).get('medium', []):
			self.terms_list.append(ObjectMedium(t_dict))
		for t_dict in self.raw_dict.get('terms', {}).get('place', []):
			self.terms_list.append(ObjectPlace(t_dict))
		for t_dict in self.raw_dict.get('terms', {}).get('century', []):
			self.terms_list.append(ObjectCentury(t_dict))
		for t_dict in self.raw_dict.get('terms', {}).get('culture', []):
			self.terms_list.append(ObjectCulture(t_dict))


	def accessionyear(self):
		return self.raw_dict.get('accessionyear')
# 		example "1969"
	def accesslevel(self):
		return self.raw_dict.get('accesslevel')
# 		example 1
	def century(self):
		return self.raw_dict.get('century')
# 		example "16th-17th century"
	def classification(self):
		return self.raw_dict.get('classification')
# 		example "Paintings"
	def colorcount(self):
		return self.raw_dict.get('colorcount')
# 		example 10
	def colors_raw(self):
		return self.raw_dict.get('colors')
	def colors(self):
		return self.colors
# 		example [ <ObjectColor>, <ObjectColor> ]
	def commentary(self):
		return self.raw_dict.get('commentary')
# 		example None
	def contact(self):
		return self.raw_dict.get('contact')
# 		example "am_asianmediterranean@harvard.edu"
	def contextualtextcount(self):
		return self.raw_dict.get('contextualtextcount')
# 		example 0
	def copyright(self):
		return self.raw_dict.get('copyright')
# 		example None
	def creditline(self):
		return self.raw_dict.get('creditline')
# 		example "Harvard Art Museums/Arthur M. Sackler Museum Gift of Eric Schroeder"
	def culture(self):
		return self.raw_dict.get('culture')
# 		example "Indian"
	def datebegin(self):
		return self.raw_dict.get('datebegin')
# 		example 1593
	def dated(self):
		return self.raw_dict.get('dated')
# 		example "c. 1598"
	def dateend(self):
		return self.raw_dict.get('dateend')
# 		example 1603
	def dateoffirstpageview(self):
		return self.raw_dict.get('dateoffirstpageview')
# 		example "2009-05-12"
	def dateoflastpageview(self):
		return self.raw_dict.get('dateoflastpageview')
# 		example "2015-03-25"
	def department(self):
		return self.raw_dict.get('department')
# 		example "Department of Islamic & Later Indian Art"
	def description(self):
		return self.raw_dict.get('description')
# 		example None
	def details(self):
		return self.details_list
	def details_raw(self):
		return self.raw_dict.get('details')
	def dimensions(self):
		return self.raw_dict.get('dimensions')
# 		example "sight: 29.85 x 16.51 cm (11 3/4 x 6 1/2 in.)"
	def division(self):
		return self.raw_dict.get('division')
# 		example "Asian and Mediterranean Art"
	def edition(self):
		return self.raw_dict.get('edition')
# 		example None
	def exhibitioncount(self):
		return self.raw_dict.get('exhibitioncount')
# 		example 0
	def groupcount(self):
		return self.raw_dict.get('groupcount')
# 		example 0
	def id(self):
		return self.raw_dict.get('id')
# 		example 216321
	def imagecount(self):
		return self.raw_dict.get('imagecount')
# 		example 2
	def imagepermissionlevel(self):
		return self.raw_dict.get('imagepermissionlevel')
# 		example 0
	def images_raw(self):
		return self.raw_dict.get('images')
	def images(self):
		return self.images
# 		example [ ]
	def lastupdate(self):
		return self.raw_dict.get('lastupdate')
# 		example "2015-04-28T04:25:01-0400"
	def markscount(self):
		return self.raw_dict.get('markscount')
# 		example 0
	def mediacount(self):
		return self.raw_dict.get('mediacount')
# 		example 0
	def medium(self):
		return self.raw_dict.get('medium')
# 		example "Opaque watercolor on paper"
	def objectid(self):
		return self.raw_dict.get('objectid')
# 		example 216321
	def objectnumber(self):
		return self.raw_dict.get('objectnumber')
# 		example "1969.172"
	def people_raw(self):
		return self.raw_dict.get('people')
	def people(self):
		return self.people
# 		example [ ]
	def peoplecount(self):
		return self.raw_dict.get('peoplecount')
# 		example 1
	def period(self):
		return self.raw_dict.get('period')
# 		example "Mughal period"
	def primaryimageurl(self):
		return self.raw_dict.get('primaryimageurl')
# 		example "http://nrs.harvard.edu/urn-3:HUAM:72721_dynmc"
	def provenance(self):
		return self.raw_dict.get('provenance')
# 		example None
	def publicationcount(self):
		return self.raw_dict.get('publicationcount')
# 		example 1
	def rank(self):
		return self.raw_dict.get('rank')
# 		example 86817
	def relatedcount(self):
		return self.raw_dict.get('relatedcount')
# 		example 0
	def signed(self):
		return self.raw_dict.get('signed')
# 		example None
	def standardreferencenumber(self):
		return self.raw_dict.get('standardreferencenumber')
# 		example None
	def state(self):
		return self.raw_dict.get('state')
# 		example None
	def style(self):
		return self.raw_dict.get('style')
# 		example None
	def technique(self):
		return self.raw_dict.get('technique')
# 		example None
	def title(self):
		return self.raw_dict.get('title')
# 		example "Kali Quaffs Blood in the Pallava Camp (painting recto; text verso) illustrated folio from a manuscript"
	def titlescount(self):
		return self.raw_dict.get('titlescount')
# 		example 1
	def totalpageviews(self):
		return self.raw_dict.get('totalpageviews')
# 		example 104
	def totaluniquepageviews(self):
		return self.raw_dict.get('totaluniquepageviews')
# 		example 78
	def url(self):
		return self.raw_dict.get('url')
# 		example "http://harvardartmuseums.org/collections/object/216321"
	def verificationlevel(self):
		return self.raw_dict.get('verificationlevel')
# 		example 2
	def verificationleveldescription(self):
		return self.raw_dict.get('verificationleveldescription')
# 		example "Adequate. Object is adequately described but information may not be vetted"
	def worktypes_raw(self):
		return self.raw_dict.get('worktypes')

from ...content_object import ContentObject

class ObjectPerson(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def birthplace(self):
		return self.raw_dict.get('birthplace')
# 		example None
	def culture(self):
		return self.raw_dict.get('culture')
# 		example None
	def deathplace(self):
		return self.raw_dict.get('deathplace')
# 		example None
	def displaydate(self):
		return self.raw_dict.get('displaydate')
# 		example None
	def displayname(self):
		return self.raw_dict.get('displayname')
# 		example "Unknown Artist"
	def displayorder(self):
		return self.raw_dict.get('displayorder')
# 		example 1
	def name(self):
		return self.raw_dict.get('name')
# 		example "Unknown Artist"
	def personid(self):
		return self.raw_dict.get('personid')
# 		example 23184
	def prefix(self):
		return self.raw_dict.get('prefix')
# 		example None
	def role(self):
		return self.raw_dict.get('role')
# 		example "Artist"

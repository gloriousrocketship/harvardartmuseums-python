from ...content_object import ContentObject

class Term(ContentObject):
	raw_dict = {}
	term_id = ""
	name = ""
	def __init__(self, dict):
		self.raw_dict = dict
		self.name = self.raw_dict["name"]
		self.term_id = dict["id"]


#terms:medium, terms:place, terms:century, terms:culture
class Medium(Term):
    name = None
    def __init__(self, dict):
		self.raw_dict = dict
    def medium_id(self):
        return self.term_id

class Place(Term):
    def __init__(self, dict):
		self.raw_dict = dict
    def place_id(self):
        return self.term_id

class Century(Term):
	def __init__(self, dict):
		self.raw_dict = dict

	def century_id(self):
		return self.term_id

class Culture(Term):
	def __init__(self, dict):
		self.raw_dict = dict
	def culture_id(self):
		return self.term_id

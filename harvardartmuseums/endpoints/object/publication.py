from ...content_object import ContentObject

class ObjectPublication(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def publicationid(self):
		return self.raw_dict.get('format')
	def publicationplace(self):
		return self.raw_dict.get('publicationplace')
	def publicationyear(self):
		return self.raw_dict.get('publicationyear')
	def publicationdate(self):
		return self.raw_dict.get('publicationdate')
	def title(self):
		return self.raw_dict.get('title')
	def citation(self):
		return self.raw_dict.get('citation')
	def citationremarks(self):
		return self.raw_dict.get('citationremarks')
	def volumetitle(self):
		return self.raw_dict.get('publicationdate')
	def volumenumber(self):
		return self.raw_dict.get('volumenumber')
	def pagenumbers(self):
		return self.raw_dict.get('publicationdate')
	def format(self):
		return self.raw_dict.get('format')

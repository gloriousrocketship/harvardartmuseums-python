from ...content_object import ContentObject

class ObjectColor(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def color(self):
		return self.raw_dict.get('color')
# 		example "#af967d"
	def css3(self):
		return self.raw_dict.get('css3')
# 		example "#bc8f8f"
	def hue(self):
		return self.raw_dict.get('hue')
# 		example "Brown"
	def percent(self):
		return self.raw_dict.get('percent')
# 		example 0.25728937728938
	def spectrum(self):
		return self.raw_dict.get('spectrum')
# 		example "#c25687"

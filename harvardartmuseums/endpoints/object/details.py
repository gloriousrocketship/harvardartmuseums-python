from ...content_object import ContentObject

class ObjectTechnicalDetails(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def text(self):
		return self.raw_dict.get('text')
	def type(self):
		return self.raw_dict.get('type')

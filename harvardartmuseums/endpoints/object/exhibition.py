from ...content_object import ContentObject

class ObjectExhibition(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def exhibitionid(self):
		return self.raw_dict.get('exhibitionid')
	def title(self):
		return self.raw_dict.get('title')
	def citation(self):
		return self.raw_dict.get('citation')
	def enddate(self):
		return self.raw_dict.get('enddate')
        #example "1968-06-30"
	def begindate(self):
		return self.raw_dict.get('begindate')
        #example "1968-06-30"

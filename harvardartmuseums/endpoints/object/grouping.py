from ...content_object import ContentObject

class ObjectGrouping(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def groupid(self):
		return self.raw_dict.get('groupid')
        #example 2039923
	def name(self):
		return self.raw_dict.get('name')
        #example "Collection Highlights"

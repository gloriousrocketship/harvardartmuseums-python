from ...content_object import ContentObject

class ObjectGallery(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def gallerynumber(self):
		return self.raw_dict.get('gallerynumber')
        #example "3400"
	def floor(self):
		return self.raw_dict.get('floor')
        #example 3
	def name(self):
		return self.raw_dict.get('name')
        #example "Ancient Mediterranean and Near Eastern Art"

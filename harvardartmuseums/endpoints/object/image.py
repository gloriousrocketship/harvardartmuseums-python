from ...content_object import ContentObject

class ObjectImage(ContentObject):
	raw_dict = {}
	def __init__(self, dict):
		self.raw_dict = dict
	def baseimageurl(self):
		return self.raw_dict.get('baseimageurl')
# 		example "http://nrs.harvard.edu/urn-3:HUAM:72721_dynmc"
	def copyright(self):
		return self.raw_dict.get('copyright')
# 		example "President and Fellows of Harvard College"
	def displayorder(self):
		return self.raw_dict.get('displayorder')
# 		example 1
	def idsid(self):
		return self.raw_dict.get('idsid')
# 		example "18481838"
	def publiccaption(self):
		return self.raw_dict.get('publiccaption')
# 		example None
	def renditionnumber(self):
		return self.raw_dict.get('renditionnumber')
# 		example "72721"

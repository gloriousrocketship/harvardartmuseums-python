from ..content_object import ContentObject

class Spectrum(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def color(self):
		return self.data_dict.get('color') #hex with "#"

	def day(self):
		return self.data_dict.get('day')

	def daynumber(self):
		return self.data_dict.get('daynumber')

	def month(self):
		return self.data_dict.get('month')

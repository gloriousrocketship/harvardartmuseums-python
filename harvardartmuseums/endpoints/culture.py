from ..content_object import ContentObject

class Culture(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:13:41-0400"  #s_to_date

	def name(self):
		return self.data_dict.get('name')

	def objectcount(self):
		return self.data_dict.get('objectcount')

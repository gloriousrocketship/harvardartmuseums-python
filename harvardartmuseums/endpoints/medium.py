from ..content_object import ContentObject

class Medium(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def mediumid(self):
		return self.data_dict.get('mediumid')

	def parentmediumid(self):
		return self.data_dict.get('parentmediumid')

	def pathforward(self):
		return self.data_dict.get('pathforward')
		
	def haschildren(self):
		return self.data_dict.get('haschildren')
# 		return 0

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:07:59-0400"  #s_to_date

	def level(self):
		return self.data_dict.get('level')
		
	def name(self):
		return self.data_dict.get('name')

	def objectcount(self):
		return self.data_dict.get('objectcount')


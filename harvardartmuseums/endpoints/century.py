from ..content_object import ContentObject

class Century(ContentObject):
	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		example "2015-04-28T04:13:45-0400"  

	def name(self):
		return self.data_dict.get('name')

	def objectcount(self):
		return self.data_dict.get('objectcount')

	def temporalorder(self):
		return self.data_dict.get('temporalorder')

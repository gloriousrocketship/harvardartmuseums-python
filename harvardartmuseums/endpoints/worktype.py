from ..content_object import ContentObject

class Worktype(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def name(self):
		return self.data_dict.get('name')

	def worktypeid(self):
		return self.data_dict.get('worktypeid')

	def objectcount(self):
		return self.data_dict.get('objectcount')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		example "2015-04-28T04:07:59-0400"

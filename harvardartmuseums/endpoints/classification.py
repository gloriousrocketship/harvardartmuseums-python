from ..content_object import ContentObject

class Classification(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def classificationid(self):
		return self.data_dict.get('classificationid')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:13:38-0400"  #s_to_date

	def name(self):
		return self.data_dict.get('name')

	def objectcount(self):
		return self.data_dict.get('objectcount')

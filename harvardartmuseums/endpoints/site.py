from ..content_object import ContentObject

class Site(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def siteid(self):
		return self.data_dict.get('siteid')

	def name(self):
		return self.data_dict.get('name')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:07:59-0400"  #s_to_date

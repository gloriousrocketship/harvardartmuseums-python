from ..content_object import ContentObject

class Place(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def placeid(self):
		return self.data_dict.get('placeid')

	def parentplaceid(self):
		return self.data_dict.get('parentplaceid')

	def haschildren(self):
		return self.data_dict.get('haschildren')
# 		return 1

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:07:59-0400"  #s_to_date

	def level(self):
		return self.data_dict.get('level')

	def name(self):
		return self.data_dict.get('name')

	def objectcount(self):
		return self.data_dict.get('objectcount')

	def pathforward(self):
		return "Europe\\"

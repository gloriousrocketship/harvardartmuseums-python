from ..content_object import ContentObject

class Color(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def colorid(self):
		return self.data_dict.get('colorid')

	def hex(self):
		return self.data_dict.get('hex')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:13:47-0400"  #s_to_date

	def name(self):
		return self.data_dict.get('name')

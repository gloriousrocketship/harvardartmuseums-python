from ..content_object import ContentObject

class Person(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def personid(self):
		return self.data_dict.get('personid')

	def displayname(self):
		return self.data_dict.get('displayname')

	def displaydate(self):
		return self.data_dict.get('displaydate')

	def gender(self):
		return self.data_dict.get('gender')

	def alphasort(self):
		return self.data_dict.get('alphasort')

	def culture(self):
		return self.data_dict.get('culture')

	def birthplace(self):
		return self.data_dict.get('birthplace')

	def datebegin(self):
		return self.data_dict.get('datebegin')
# 		return 1880

	def dateend(self):
		return self.data_dict.get('dateend')
# 		return 1960

	def deathplace(self):
		return self.data_dict.get('deathplace')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:07:59-0400"  #s_to_date

	def objectcount(self):
		return self.data_dict.get('objectcount')

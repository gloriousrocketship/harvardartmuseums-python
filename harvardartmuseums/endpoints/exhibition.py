from ..content_object import ContentObject

class Exhibition(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def exhibitionid(self):
		return self.data_dict.get('exhibitionid')

	def begindate(self):
		return self.data_dict.get('begindate')

	def color(self):
		return self.data_dict.get('color')

	def description(self):
		return self.data_dict.get('description')

	def enddate(self):
		return self.data_dict.get('enddate')
# 		return "2015-04-28T04:10:07-0400"  #s_to_date

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:10:07-0400"  #s_to_date

	def primaryimageurl(self):
		return self.data_dict.get('primaryimageurl')

	def shortdescription(self):
		return self.data_dict.get('shortdescription')

	def temporalorder(self):
		return self.data_dict.get('temporalordertemporalorder')

	def title(self):
		return self.data_dict.get('title')
# 		return "Exhibition et vente de 40 tableaux et 4 dessins de l'oeuvre de M. Gustave Courbet"

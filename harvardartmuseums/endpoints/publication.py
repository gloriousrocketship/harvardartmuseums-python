from ..content_object import ContentObject

class Publication(ContentObject):
	def id(self):
		return self.data_dict.get('id')

	def publicationid(self):
		return self.data_dict.get('publicationid')

	def publicationplace(self):
		return self.data_dict.get('publicationplace')

	def publicationdate(self):
		return self.data_dict.get('publicationdate')
# 		return "Winter 1961"

	def publicationyear(self):
		return self.data_dict.get('publicationyear')
# 		return 1961

	def citation(self):
		return self.data_dict.get('citation')
# 		return "[Unidentified article]<em>News</em> (Winter 1961)vol. XXIVno. 2"

	def description(self):
		return self.data_dict.get('description')

	def format(self):
		return self.data_dict.get('format')

	def lastupdate(self):
		return self.data_dict.get('lastupdate')
# 		return "2015-04-28T04:07:59-0400"  #s_to_date

	def objectcount(self):
		return self.data_dict.get('objectcount')

	def primaryimageurl(self):
		return self.data_dict.get('primaryimageurl')

	def shortdescription(self):
		return self.data_dict.get('shortdescription')

	def title(self):
		return self.data_dict.get('title')

	def volumenumber(self):
		return self.data_dict.get('volumenumber')

	def volumetitle(self):
		return self.data_dict.get('volumetitle')

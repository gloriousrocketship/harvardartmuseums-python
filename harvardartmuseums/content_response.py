from PIL import Image #pip install Pillow
import requests #pip install requests
from StringIO import StringIO
#### ^ For Images
import json, urllib
import globals, errors
from content_request import ContentRequest

class ContentResponse():
	"""internal class for handling a content repsonse"""
	request_data = None #ContentRequest
	response_raw = ''
	response_dict = { }
	response_objs = [ ] #ContentObject(s)

	def __init__(self, requestData):
		self.request_data = requestData
		self.refresh()

	def __str__(self):
		return self.response_raw

	def refresh(self):
		if self.request_data.connection.sock != None :
			self.request_data.connection.close()
		self.response_raw = self.request_data.send().read().decode('utf-8')
		self.response_dict = json.loads(self.response_raw)
		#create_response_data
		class_to_cast = classFromEndpoint(self.request_data.endpoint)
		self.response_objs = []
		for object_dict in self.response_dict.get('records'): #casting the records by type
			object_class = class_to_cast(self.response_dict, object_dict)
			self.response_objs.append(object_class)
		return self


	def endpoint(self):
		return self.request_data.endpoint.split("?")[0]

	def content_objects(self):
		return self.response_objs

	def records(self):
		return self.response_dict.get('records')

	def info_dict(self):
		return self.response_dict.get('info', {})
	def page(self):
		return int(self.info_dict().get('page'))
	def pages(self):
		return int(self.info_dict().get('pages'))
	def totalrecords(self):
		return int(self.info_dict().get('totalrecords'))

	def imageOfUrl(self, url=""):
		img_response = requests.get(url)
		img = Image.open(StringIO(img_response.content))
		return img

	def nextPageVal(self):
		'''returns the number of the next page, or the same value if we are at the end of the pages. Useful for display purposes'''
		nextPage = self.page()+1
		if nextPage > self.pages():
			nextPage = self.pages()
		return nextPage

	def previousPageVal(self):
		'''returns the number of the previous page, or the same value if we are at page 1. Useful for display purposes'''
		prevPage = self.page()-1
		if prevPage == 0:
			prevPage = 1
		return prevPage

	def nextPage(self):
		self.request_data.page(self.nextPageVal())
		return self.refresh()

	def previousPage(self):
		self.request_data.page(self.previousPageVal())
		return self.refresh()

def classFromEndpoint(endpoint):
	import endpoints as m
	if globals.ep_activity in endpoint:
		return m.activity.Activity
	elif globals.ep_century in endpoint:
		return m.century.Century
	elif globals.ep_classification in endpoint:
		return m.classification.Classification
	elif globals.ep_color in endpoint:
		return m.color.Color
	elif globals.ep_culture in endpoint:
		return m.culture.Culture
	elif globals.ep_exhibition in endpoint:
		return m.exhibition.Exhibition
	elif globals.ep_gallery in endpoint:
		return m.gallery.Gallery
	elif globals.ep_group in endpoint:
		return m.group.Group
	elif globals.ep_medium in endpoint:
		return m.medium.Medium
	elif globals.ep_object in endpoint:
		return m.object.Object
	elif globals.ep_period in endpoint:
		return m.period.Period
	elif globals.ep_person in endpoint:
		return m.person.Person
	elif globals.ep_place in endpoint:
		return m.place.Place
	elif globals.ep_publication in endpoint:
		return m.publication.Publication
	elif globals.ep_site in endpoint:
		return m.site.Site
	elif globals.ep_spectrum in endpoint:
		return m.spectrum.Spectrum
	elif globals.ep_technique in endpoint:
		return m.technique.Technique
	elif globals.ep_worktype in endpoint:
		return m.worktype.Worktype
	else:
		raise errors.HarvardApiError("Endpoint not defined {}".format(endpoint))
		return None

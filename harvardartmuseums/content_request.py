#import http.client as httpclient #python 3.x
import httplib as httpclient   #python 2.7
import json, urllib, string, sys
from urlparse import urlparse
import globals, errors

class ContentRequest():
	"""internal class for editing/sending a request with parameters"""
	connection = None
	endpoint = None
	request_dict = None

	def __init__(self, endpoint):
		if globals.client_key is None or len(globals.client_key) == 0:
			raise HarvardApiError(errors.no_client_key)
		self.connection = httpclient.HTTPConnection(globals.endpoint)
		self.request_dict = {'apikey': globals.client_key}
		self.endpoint = endpoint

	def __str__(self):
		return self.endpoint + "\t" + str(self.request_dict)

	def set_and_return(self, key, value):
		self.request_dict[key] = value
		return self
	def update_params(self, new_params):
		self.request_dict.update(new_params)
		return self

	def send(self):
		just_the_endpoint = string.split(self.endpoint, "?")[0] #strip  any old parameters from the endpoint
		self.endpoint = just_the_endpoint + "?" + (urllib.urlencode(self.request_dict)) #add raw request to the end of the url
		request_json = json.dumps(self.request_dict)  #add raw request dictionary as json
		self.connection.request('GET', self.endpoint, request_json, {'Content-type': 'application/json'})
		return self.connection.getresponse()

	"""custom query fuction"""
	def q(self, field, value):
		return self.set_and_return('q', (str(field)+":"+str(value)))
	"""number of results to return (valid range not known)"""
	def size(self, s):
		return self.set_and_return('size', s)
	"""page index of results, returns first page if not used"""
	def page(self, p):
		return self.set_and_return('page', p)

	def venue(self,ham):
		return self.set_and_return('venue', ham)
	def exact_title(self,url_encoded_title):
		return self.set_and_return('exact_title', url_encoded_title)
	def usedby(self, field_name,value):
		return self.set_and_return('usedby', (str(field_name)+":"+str(value)))
	def object(self, object_id):
		return self.set_and_return('object', str(object_id))
	def aggregation(self, aggregation_string):
		return self.set_and_return('aggregation', str(aggregation_string))
	def type(self, t):
		return self.set_and_return('type', str(t))
	#sort order functions
	def sort(self, s):
		return self.set_and_return('sort', s)
	def sortOrder(self, s):
		return self.set_and_return('sortorder', s)
	def randomize(self, seed=None):
		randomVar = "random"
		if seed is not None :
			randomVar="random:"+str(seed)
		return self.sort(randomVar)
	def asc(self):
		return self.sortOrder('asc')
	def desc(self):
		return self.sortOrder('desc')
	#image functions
	def hasimage(self,oneOrZero):
		if oneOrZero != 0 and oneOrZero != 1:
			raise HarvardApiError(errors.incorrect_value(oneOrZero))
		return self.set_and_return('hasimage', oneOrZero)
	def onlyImages(self):
		return self.hasimage(1)
	def noImages(self):
		return self.hasimage(0)
	#YYYY-MM-DD begindate:YYYY-MM-DD enddate:YYYY-MM-DD
	#TODO format temporalorder:1234
	def before(self,date,prefix=''):
		date_string = date
		if len(prefix) > 0:
			date_string=prefix+":"+date_string
		return self.set_and_return('before', date_string)
	def after(self,date,prefix=''):
		date_string = date
		if len(prefix) > 0:
			date_string=prefix+":"+date_string
		return self.set_and_return('after', date_string)
	def gallery_floor(self,floor):
		return self.set_and_return('floor', floor)
	def group_level(self,level):
		return self.set_and_return('level', level)
	def group_parent(self,parent):
		return self.set_and_return('parent', parent)
	def fields(self,fields):
		fieldstring = ','.join(fields)
		return self.set_and_return('fields', fieldstring)
	# OBJECT Parameter	Value
	#century			CENTURY or '|' (pipe) separated list of centuries or "any"
	def century(self, var):
		return self.adaptive_set('century', var)
	# classification	CLASSIFICATION or '|' (pipe) separated list of classifications or "any"
	def classification(self, var):
		return self.adaptive_set('classification', var)
	# color	     		COLOR or '|' (pipe) separated list of colors or "any"
	def color(self, var):
		return self.adaptive_set('color', var)
	# culture	     	CULTURE or '|' (pipe) separated list of cultures or "any"
	def culture(self, var):
		return self.adaptive_set('color', var)
	# exhibition	 	EXHIBITION ID or "any"
	def exhibition(self, var):
		return self.adaptive_set('color', var)
	# gallery			GALLERY NUMBER or '|' (pipe) separated list of gallery numbers or "any"
	def gallery(self, var):
		return self.adaptive_set('color', var)
	# group				GROUP NAME or "any"
	def group(self, var="any"):
		return self.set_and_return('group', var)
	# medium			MEDIUM or '|' (pipe) separated list of mediums or "any"
	def medium(self, var):
		return self.adaptive_set('color', var)
	# objectnumber		1 or more terms
	def objectnumber(self, var):
		return self.set_at_least_one('objectnumber',var)
	# period			PERIOD or '|' (pipe) separated list of periods or "any"
	def period(self, var):
		return self.adaptive_set('color', var)
	# person			PERSON ID or PERSON NAME or "any"
	def person(self, var):
		return self
	# place				PLACE ID or PLACE or '|' (pipe) separated list of values or "any"
	def place(self, var):
		return self
	# technique			TECHNIQUE or '|' (pipe) separated list of techniques or "any"
	def technique(self, var):
		return self.adaptive_set('color', var)
	# worktype			WORKTYPE or '|' (pipe) separated list of worktypes or "any"
	def worktype(self, var):
		return self.adaptive_set('color', var)
	#********************************************************
	#********************************************************
	# Might be obsolete? Consider revising/removing.
	# title				1 or more terms
	def title(self, var):
		return self.set_at_least_one('title',var)
	# keyword			1 or more terms
	def keyword(self, var):
		return self.set_at_least_one('keyword',var)
	# relatedto			OBJECT ID
	def relatedto(self, var):
		return self.set_and_return('relatedto', var)

def get_endpoint(endpoint, request_params={}):
	"""returns an editable content request object for the endpoint and parameters.
	Throws an error if client_key is not defined."""
	return  ContentRequest(endpoint).update_params(request_params)
